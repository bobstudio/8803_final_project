# You want to make sure your version produces better error rates than this :)

import sys

"""Problem Set 7: Particle Filter Tracking."""

import numpy as np
import cv2
import math
import random

import os

# I/O directories
input_dir = "input"
output_dir = "output"



def showIm(im, time=1, name="im"):
    img_out = cv2.normalize(im, 0, 255, norm_type=cv2.cv.CV_MINMAX).astype(np.uint8)
    cv2.imshow(name, img_out)
    cv2.waitKey(time)

# Assignment code
class ParticleFilter(object):
    """A particle filter tracker, encapsulating state, initialization and update methods."""
    
    def __init__(self, p, template, **kwargs):
        """Initialize particle filter object.
            
            Parameters
            ----------
            frame: color BGR uint8 image of initial video frame, values in [0, 255]
            template: color BGR uint8 image of patch to track, values in [0, 255]
            kwargs: keyword arguments needed by particle filter model, including:
            - num_particles: number of particles
            """
        self.max_num_particles = kwargs.get('max_num_particles', 100)
        self.min_num_particles = kwargs.get('min_num_particles', 10)
        
        x,y = p
        self.template = (x,y)
        self.sigma_mse = kwargs.get('sigma_mse', 10)
        self.sigma_dynamic = kwargs.get('sigma_dynamic', 15)
        self.min_sigma_dynamic = kwargs.get('min_sigma_dynamic', 3)
        self.max_sigma_dynamic = kwargs.get('max_sigma_dynamic', 300)
        
        h,w = frame.shape[:2]
        self.particles = np.array([np.array([self.get_uniform_val(h),self.get_uniform_val(w), 1./self.max_num_particles]) for i in range(self.max_num_particles)])
    
    def get_uniform_val(self, max):
        return int(math.floor(random.uniform(0,max)))
    
    def mse(self, frame, up, vp):
        m,n = self.template.shape
        top = up
        left = vp
        image_patch = frame[top:top+m, left:left+n]
        dt = np.square(self.template - image_patch)
        return np.sum(dt)/(m*n)
    
    def p_st_xt(self, frame, up, vp):
        sim = math.exp(-self.mse(frame, up,vp)/(2*(self.sigma_mse**2)))
        return sim
    
    def single_channel(self, frame):
        WEIGHT_B = .12
        WEIGHT_G = .58
        WEIGHT_R = .3
        return frame[:,:,0]*WEIGHT_B + frame[:,:,1]*WEIGHT_G + frame[:,:,2]*WEIGHT_R
    
    def resample(self):
        dev = self.find_deviation(self.particles)
        m,n = self.template.shape[:2]
        if dev > min(m,n)/2:
            n_samples = np.clip(1.1*len(self.particles), self.min_num_particles, self.max_num_particles)
        else:
            n_samples = np.clip(.9*len(self.particles), self.min_num_particles, self.max_num_particles)
        
        votes = np.random.multinomial(n_samples, self.particles[:,2], size=1).flatten()
        new_particles = []
        for i in range(len(votes)):
            for j in range(votes[i]):
                new_particles.append(self.particles[i].copy())
        
        return np.array(new_particles)

    def add_noise(self, frame):
        hh,ww,_ = frame.shape
            n_samples = len(self.particles)
            self.particles[:,0] = np.mod(self.particles[:,0] + np.random.normal(0,self.sigma_dynamic,n_samples) ++ hh, hh)
            self.particles[:,1] = np.mod(self.particles[:,1] + np.random.normal(0,self.sigma_dynamic,n_samples) + ww, ww)

    def process(self, frame):
        """Process a frame (image) of video and update filter state.
            
            Parameters
            ----------
            frame: color BGR uint8 image of current video frame, values in [0, 255]
            """
        self.particles = self.resample()
        self.add_noise(frame);
        
        # Weighted sum of R, B, G
        image = self.single_channel(frame).astype(float)*1/255.
        m,n = self.template.shape
        image = np.lib.pad(image, ((m/2,m/2),(n/2,n/2)), 'edge')
        
        for p in self.particles:
            y,x,w = p
            p[2] = self.p_st_xt(image, y,x)
        self.particles[:,2] /= np.sum(self.particles[:,2])

    def find_weighted_mean(self, particles):
        u = 0
        v = 0
        for p in particles:
            y,x,w = p
            u += w*y
            v += w*x
        return u,v

    def find_deviation(self, particles):
        u,v = self.find_weighted_mean(particles)
        du = np.square(particles[:,0]-u)
        dv = np.square(particles[:,1]-v)
        return int(np.sum(np.sqrt(du+dv)*particles[:,2]))
    
    def render(self, frame_out):
        """Visualize current particle filter state.
            
            Parameters
            ----------
            frame_out: copy of frame to overlay visualization on
            """
        # particle locations
        for p in self.particles:
            y,x,weight = p
            r = 1#int(weight*300)
            cv2.circle(frame_out, (int(x),int(y)), r, (0,255,255))
        
        # weighted mean
        u,v = self.find_weighted_mean(self.particles)
        h,w = self.template.shape[:2]
        left = int(v-w/2)
        top = int(u-h/2)
        cv2.rectangle(frame_out, (left,top), (left+w,top+h), (0,255,255))
        
        # deviation
        r = self.find_deviation(self.particles)
    cv2.circle(frame_out, (int(v),int(u)), int(r), (0,0,255),thickness=2)


class AppearanceModelPF(ParticleFilter):
    """A variation of particle filter tracker that updates its appearance model over time."""
    
    def __init__(self, frame, template, **kwargs):
        """Initialize appearance model particle filter object (parameters same as ParticleFilter)."""
        super(AppearanceModelPF, self).__init__(frame, template, **kwargs)
        self.alpha = kwargs.get('alpha', .5)
        self.weight_threshold = kwargs.get('weight_threshold', 2)
   
   def find_best_template(self, p):
        max_weight_ind = np.where(self.particles[:,2] == np.max(self.particles[:,2]))[0][0]
        y,x,w = self.particles[max_weight_ind]
        
        top = y
        left = x
        best_template = (y,x)

        if w < (1./len(self.particles))*self.weight_threshold:
            return self.template
        return best_template
    
    def get_new_template(self, old_template, best_template):
        new_template = self.alpha * best_template + (1 - self.alpha) * old_template
        return new_template
    
    def process(self, p):
        super(AppearanceModelPF, self).process(p)
        self.template = self.get_new_template(self.template, self.find_best_template(image))


def run_particle_filter(pf_class, inpu_filename, **kwargs):
    """Instantiate and run a particle filter on a given video and template.
        
        Create an object of type pf_class, passing in initial video frame,
        template (extracted from first frame using template_rect), and any keyword arguments.
        
        Parameters
        ----------
        pf_class: particle filter class to instantiate (e.g. ParticleFilter)
        video_filename: path to input video file
        template_rect: dictionary specifying template bounds (x, y, w, h), as float or int
        save_frames: dictionary of frames to save {<frame number>|'template': <filename>}
        kwargs: arbitrary keyword arguments passed on to particle filter class
        """
    
    # Open video file
    xy = []
    for l in open(filename, 'r').readlines():
        x, y = l.split(',')
        xy += [(x,y)]
    
    # Initialize objects
    pf = None
    frame_num = 0
    
    # Loop over video (till last frame or Ctrl+C is presssed)
    for p in xy:
        # Process frame
        pf.process(p)
        #pf.predict()
        # Render and save output, if indicated


def main():
    run_particle_filter(AppearanceModelPF,
                        os.path.join(input_dir, "test01.txt"),
                        max_num_particles=1000,
                        min_num_particles=50,
                        sigma_mse = 0.03,
                        sigma_dynamic = 20,
                        alpha = .15,
                        weight_threshold = 10)

if __name__ == "__main__":
    main()
#
#
#
#filename = sys.argv[1]
#x, y = open(filename, 'r').readlines()[-1].split(',')
#print x
#print y
#with open('prediction.txt', 'w') as f:
#    for _ in range(60):
#        print >> f, '%s,%s' % (x.strip(), y.strip())
